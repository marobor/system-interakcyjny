<?php
/**
 * Post fixtures.
 */

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class PostFixtures.
 */
class PostFixtures extends AbstractBaseFixtures implements DependentFixtureInterface
{
    /**
     * Load.
     *
     * @param \Doctrine\Persistence\ObjectManager $manager Persistence object manager
     */
    public function loadData(ObjectManager $manager): void
    {
        $this->createMany(100, 'posts', function ($i) {
            $post = new Post();
            $post->setTitle($this->faker->sentence);
            $post->setAuthor($this->faker->name);
            $post->setContent($this->faker->sentence);
            $post->setDate($this->faker->dateTimeBetween('-100 days', '-1 days'));

            $post->setCategory($this->getRandomReference('categories'));



            return $post;
        });

        $this->manager->flush();
    }

    public function getDependencies()
    {
        return [CategoryFixtures::class];
    }
}
