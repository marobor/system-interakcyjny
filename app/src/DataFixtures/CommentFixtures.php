<?php
/**
 * Comment fixtures.
 */

namespace App\DataFixtures;

use App\Entity\Comment;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class CommentsFixtures.
 */
class CommentFixtures extends AbstractBaseFixtures implements DependentFixtureInterface
{
    /**
     * Load.
     *
     * @param \Doctrine\Persistence\ObjectManager $manager Persistence object manager
     */
    public function loadData(ObjectManager $manager): void
    {
        $this->createMany(10, 'comments', function ($i) {
            $comment = new Comment();
            $comment->setCommentContent($this->faker->sentence);
            $comment->setNick($this->faker->name);
            $comment->setEmail($this->faker->email);

            $comment->setPost($this->getRandomReference('posts'));

            return $comment;
        });

        $this->manager->flush();
    }

    public function getDependencies()
    {
        return [CategoryFixtures::class, PostFixtures::class];
    }
}