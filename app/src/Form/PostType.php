<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Post;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            [
                'label' => 'label_title',
                'required' => true,
                'attr' => ['max_lenght => 64'],
            ]
        );
        $builder->add(
            'content',
            TextareaType::class,
            [
                'label' => 'label_content',
                'required' => true,
                'attr' => ['max_lenght => 400'],
            ]
        );
        $builder->add(
            'author',
            TextType::class,
            [
                'label' => 'label_author',
                'required' => true,
                'attr' => ['max_lenght => 64'],
            ]
        );
        $builder->add(
            'date',
            DateType::class,
            [
                'label' => 'label_date',
                'placeholder' => [
                    'year' => 'YYYY',
                    'month' => 'MM',
                    'day' => 'DD',
                ],
                'required' => true,
            ]
        );
        $builder->add(
            'category',
            EntityType::class,
            [
                'class' => Category::class,
                'choice_label' => function ($category) {
                    return $category->getName();
                },
                'label' => 'label_name',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Post::class]);
    }

    public function getBlockPrefix()
    {
        return 'post';
    }
}
