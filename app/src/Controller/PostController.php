<?php
/**
 * Post Controller.
 */

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Form\PostType;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostController.
 *
 * @Route("/post")
 */
class PostController extends AbstractController
{
    private PostRepository $postRepository;

    private PaginatorInterface $paginator;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     * @param PaginatorInterface $paginator
     */
    public function __construct(PostRepository $postRepository, PaginatorInterface $paginator)
    {
        $this->postRepository = $postRepository;
        $this->paginator = $paginator;
    }


    /**
     * Index action.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request        HTTP request
     * @param \App\Repository\PostRepository            $postRepository Post repository
     * @param \Knp\Component\Pager\PaginatorInterface   $paginator      Paginator
     *
     * @return \Symfony\Component\HttpFoundation\Response               HTTP response
     *
     * @Route (
     *     "/",
     *     methods={"GET"},
     *     name="post_index",
     *)
     */
    public function index(Request $request, PostRepository $postRepository, PaginatorInterface $paginator): Response
    {
        $pagination = $paginator->paginate(
            $postRepository->queryAll(),
            $request->query->getInt('page', 1),
                PostRepository::PAGINATOR_ITEMS_PER_PAGE
        );


        return $this->render(
            'post/index.html.twig',
            ['pagination' => $pagination]

        );
    }

    /**
     * Show Action.
     *
     * @param int $id Record id
     *
     * @return \Symfony\Component\HttpFoundation\Response HTTP Response
     *
     * @Route(
     *     "/{id}",
     *     methods={"GET"},
     *     name="post_show",
     *     requirements={"id": "[1-9]\d*"},
     * )
     */
    public function show(Post $post, CommentRepository $commentRepository): Response
    {
        $comments = $post->getComments();

        return $this->render(
            'post/show.html.twig', [
                'post' => $post,
                'comments' => $comments
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request    HTTP Request
     *
     * @return \Symfony\Component\HttpFoundation\Response           HTTP Response
     *
     * @Route(
     *     "/{id}/comment",
     *     methods={"GET", "POST"},
     *     name="comment",
     * )
     */
    public function comment(Request $request): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->postRepository->save($comment);

            $this->addFlash('success', 'message_created_successfully');

            return $this->redirectToRoute('post_show');
        }

        return $this->render(
            'post/comment.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request    HTTP Request
     *
     * @return \Symfony\Component\HttpFoundation\Response           HTTP Response
     *
     * @Route(
     *     "/create",
     *     methods={"GET", "POST"},
     *     name="post_create",
     * )
     */
    public function create(Request $request): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->postRepository->save($post);

            $this->addFlash('success', 'message_created_successfully');

            return $this->redirectToRoute('post_index');
        }

        return $this->render(
            'post/create.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request     $request        HTTP request
     * @param \App\Repository\PostRepository                $post           Post entity
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route(
     *      "/{id}/edit",
     *     methods={"GET", "PUT"},
     *     name="post_edit",
     *     requirements={"id": "[1-9]\d*"},
     * )
     */
    public function edit(Request $request, Post $post): Response
    {
        $form = $this->createForm(PostType::class, $post, ['method' => 'PUT']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->postRepository->save($post);

            $this->addFlash('success', 'message_created_successfully');

            return $this->redirectToRoute('post_index');
        }

        return $this->render(
            'post/edit.html.twig',
            [
                'form' => $form->createView(),
                'post' => $post,
            ]
        );
    }

    /**
     * Delete action.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request            HTTP request
     * @param \App\Entity\Post                          $post               Post entity
     * @param \App\Repository\PostRepository            $postRepository     Post repository
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @Route(
     *      "/{id}/delete",
     *     methods={"GET", "DELETE"},
     *     name="post_delete",
     *     requirements={"id": "[1-9]\d*"},
     * )
     */
    public function delete(Request $request, Post $post): Response
    {
        $form = $this->createForm(FormType::class, $post, ['method' => 'DELETE']);
        $form->handleRequest($request);

        if($request->isMethod('DELETE') && !$form->isSubmitted()) {
            $form->submit($request->request->get($form->getName()));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->postRepository->delete($post);

            $this->addFlash('success', 'message_deleted_successfully');

            return $this->redirectToRoute('post_index');
        }

        return $this->render(
            'post/delete.html.twig',
            [
                'form' => $form->createView(),
                'post' => $post,
            ]
        );
    }
}
